package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.GameError;
import controller.HangmanController;
import data.GameData;
import data.Matrix;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author VICTOR TELLEZ
 * @author Ritwik Banerjee
 */


public class Workspace extends AppWorkspaceComponent {









    public final int  listSize = 100;
    public final int matrixSize1D = 4;

    AppTemplate          app; // the actual application
    AppGUI               gui; // the GUI inside which the application sits
    HangmanController    controller;
    HBox                 bodyPane;          //green container for the main game displays
    VBox                 gameTextsPane;     //pink container to display the text-related parts of the game
    ScrollPane           guessedLetters;    //black text area displaying all the letters guessed so far
    ScrollPane           help;
    HBox                 remainingGuessBox; //purple container to display the number of remaining guesses
    VBox                 figurePane;        //orange container to display the namesake graphic of the (potentially) hanging person
    HBox                 optionsGui;
    VBox                 toolColumn;
    VBox                 titles;
    HBox                 title;
    HBox                 subtitle1;
    HBox                 subtitle2;
    GridPane             matrixGui;
    VBox                 blBox;             //bottom left box in gui
    ToolBar              footToolbar;       //yellow toolbar for game buttons
    Label                guiHeadingLabel;   //blue workspace (GUI) heading label
    HBox                 headPane;          //red conatainer to display the heading
    Text[]      matrixBox = new Text[(matrixSize1D*matrixSize1D)];
    Rectangle[]  blue = new Rectangle[matrixSize1D*matrixSize1D];
    Rectangle[] green = new Rectangle[matrixSize1D*matrixSize1D];

    TextField userNameFld = new TextField();
    PasswordField passwordFld = new PasswordField();
    PasswordField passwordFld2 = new PasswordField();
    String keyWord;
    Button                 startGame;         //brown the button to start playing a game of Hangman
    Button                  newButt                                     = new Button("Create New Profile");
    Button                  logInButt                                   = new Button("Log in");
    Button                  logOutButt                                  = new Button("Log Out");
    Button                  editUsernameButt                            = new Button("Make this my new Name");
    Button                  editPasswordButt                            = new Button("Make this my new Password");

    Button                  replayButt                                  = new Button("Replay Level");
    boolean                 secret=!true;


    VBox guesses;
    StackPane stack;
    String[][] letters = new String[matrixSize1D][matrixSize1D];

    boolean pause = false;
    static Matrix matrix = new Matrix();
    String board = "1234567890123456";

    ObservableList<String> LevelOptions = FXCollections.observableArrayList("1", "2", "3", "4", "5", "6", "7", "8");
    ObservableList<String> modeOptions = FXCollections.observableArrayList("Mode 1", "Mode 2", "Mode 3", "Mode 4");





    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();
        layoutGUI();
        setupHandlers();
    }

    //   *******************************************************
    //************************GETTERS********************************
    public PasswordField getPasswordFld() {
        return passwordFld;
    }
    public PasswordField getPasswordFld2() {
        return passwordFld2;
    }
    public VBox getblBox() {
        return blBox;
    }
    public VBox gettitles() {
        return titles;
    }
    public VBox getGameTextsPane() {
        return gameTextsPane;
    }
    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }
    public ScrollPane getGuessedLetters() {
        return guessedLetters;
    }
    public Button getStartGame() {
        return startGame;
    }
    public TextField getUserNameFld() {
        return userNameFld;
    }
    public ObservableList<String> getLevelOptions() {
        return LevelOptions;
    }
    public ObservableList<String> getModeOptions() {
        return modeOptions;
    }
    public AppTemplate getApp() {
        return app;
    }
    public AppGUI getGui() {
        return gui;
    }
    public HBox getBodyPane() {
        return bodyPane;
    }
    public VBox getFigurePane() {
        return figurePane;
    }
    public HBox getOptionsGui() {
        return optionsGui;
    }
    public VBox getToolColumn() {
        return toolColumn;
    }
    public VBox getTitles() {
        return titles;
    }
    public HBox getTitle() {
        return title;
    }
    public HBox getSubtitle1() {
        return subtitle1;
    }
    public HBox getSubtitle2() {
        return subtitle2;
    }
    public GridPane getMatrixGui() {
        return matrixGui;
    }
    public VBox getBlBox() {
        return blBox;
    }
    public ToolBar getFootToolbar() {
        return footToolbar;
    }
    public Label getGuiHeadingLabel() {
        return guiHeadingLabel;
    }
    public HBox getHeadPane() {
        return headPane;
    }
    public HangmanController getController() {
        return controller;
    }
    public StackPane getStack() {
        return stack;
    }
    public boolean isPause() {
        return pause;
    }
    public Text[] getMatrixBox() {
        return matrixBox;
    }
    public Rectangle[] getBlue() {
        return blue;
    }
    public Rectangle[] getGreen() {
        return green;
    }
    public String[][] getLetters() {
        return letters;
    }
    public int getWordLength(){
        return keyWord.length();
    }
    public String getWord(){
        return keyWord;
    }
    public boolean isSecret() {
        return secret;
    }
    public void setSecret(boolean secret) {
        this.secret = secret;
    }

    public void setBoard(String board) {
        this.board = board;
    }
    public void placeInTitle(Node title) {
        this.title.getChildren().setAll(title);
    }
    public void placeInSubtitle1(Node subtitle1) {
        this.subtitle1.getChildren().setAll(subtitle1);
    }
    public void placeInSubtitle2(Node subtitle2) {
        this.subtitle2.getChildren().setAll(subtitle2);
    }
    public String lineMaker(char[] line, boolean reverse){
        char[] zero = new char[matrixSize1D];
        Random r = new Random();
        if(reverse){
            if(Character.isLetter(line[3])) zero[0]= line[3];
            else{zero[0] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[2])) zero[1]= line[2];
            else{zero[1] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[1])) zero[2]= line[1];
            else{zero[2] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[0])) zero[3]= line[0];
            else{zero[3] = (char)(r.nextInt(26) + 'a');}
        }else{
            if(Character.isLetter(line[3])) zero[0]= line[0];
            else{zero[0] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[2])) zero[1]= line[1];
            else{zero[1] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[1])) zero[2]= line[2];
            else{zero[2] = (char)(r.nextInt(26) + 'a');}
            if(Character.isLetter(line[0])) zero[3]= line[3];
            else{zero[3] = (char)(r.nextInt(26) + 'a');}
        }
        return String.valueOf(zero);
    }
    private void noStart() {startGame.setVisible(false);}
    private void yesStart() {startGame.setVisible(true);}
    public Text textMaker(String text, int size) {
        Text x = new Text(text);
        x.setFont(Font.font("Verdana", FontWeight.BOLD, size));
        return x;
    }
    public void gameBoardGenerator(String word) {
        matrix.placeWord(word);
        int a= 0;
        matrixGui=new GridPane();
        for (int i = 0; i <4; i++) {
            for (int j = 0; j < 4; j++) {
                a++;
                matrixGui.add((matrix.findNode(a).getContent()), j, i);
                matrixGui.setMargin((matrix.findNode(a).getContent()), new Insets(5, 5, 5, 5));
                System.out.println("THE NODE: "+a+"    COORDINATES :  "+i+j);
                matrixGui.setAlignment(Pos.CENTER);
            }
        }
    }












    public String findWordFromList() {
        URL wordsResource = getClass().getClassLoader().getResource("words/colors.txt");
        assert wordsResource != null;
        int toSkip = new Random().nextInt(listSize);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word. /n/nfindWordFromList/n/n");
    }
    public String findWordFromListGivenName(String Mode) {
        URL wordsResource = getClass().getClassLoader().getResource("words/"+Mode+".txt");
        assert wordsResource != null;
        int toSkip = new Random().nextInt(listSize);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        throw new GameError("Unable to load initial target word. /n/nfindWordFromListGivenName(String Mode)/n/n");
    }


    private void pauseRedoMatrixButton(String[][] letters) {
        int k = 0;
        for (int i = 0; i < matrixSize1D; i++) {
            for (int j = 0; j <  matrixSize1D; j++) {
                stack = new StackPane();
                matrixBox[k] = textMaker(letters[i][j], 20);
                blue[k] = new Rectangle(50, 50, Color.LIGHTBLUE);
                green[k] = new Rectangle(50, 50, Color.LIGHTGREEN);
                matrixBox[k].setVisible(!true);
                blue[k].setVisible(true);
                green[k].setVisible(!true);
                stack.getChildren().addAll(blue[k], green[k], matrixBox[k]);
                matrixGui.add(stack, j, i);
                matrixGui.setMargin(stack, new Insets(5, 5, 5, 5));
                k++;
            }
        }
        matrixGui.setAlignment(Pos.CENTER);
    }
    private void redoMatrixText(String[][] letters) {
        for (int i = 0; i <  matrixSize1D; i++) {
            for (int j = 0; j <  matrixSize1D; j++) {
                matrixGui.add(new Text(letters[i][j]), i, j);
            }
        }
    }
    public String[][] lettersSetup(String string) {
        char[] helper = string.toCharArray();
        int k = 0;
        for (int i = 0; i < matrixSize1D; i++) {
            for (int j = 0; j < matrixSize1D; j++) {
                letters[i][j] = String.valueOf(helper[k]);
                k++;
            }
        }
        return letters;
    }
    public String levelString(GameData gameData, int num) {
        switch (num) {
            case 1:
                System.out.println(gameData.getMode1().iterator().toString());
                break;
            case 2:
                System.out.println(gameData.getMode1().iterator());

                break;
            case 3:
                System.out.println(gameData.getMode1().iterator());
                break;
            case 4:
                System.out.println(gameData.getMode1().iterator());
                break;
            default:
            return "nothing 2 c here";
        }
        return "nothing to see here please log in";
    }

    //***************************************************************
    //    ******************************************************

    public void handleLogInButt() {
        try {
            String password=getPasswordFld().getText();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                     endMessage = String.format("account DOES NOT exist\n Make a new account");

            if (!password.equals(controller.password)) {
                secret=true;
                if (dialog.isShowing())
                    dialog.toFront();
                else
                    dialog.show("LOG", endMessage);
                System.out.println("PASSWORDS1:"+(getPasswordFld().getText())+"PASSWORDS2:"+(controller.password));
            }else {
                endMessage = String.format("LOG-ED IN SUCCESFULL");
                secret=false;
                //load user data@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


                if (dialog.isShowing()) dialog.toFront();
                else dialog.show("LOG", endMessage);




            }
        }catch(NullPointerException e){
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("ERROR unfilled areas","Please fill out all the boxes before pressing the Button");
            System.out.println("NewButt HAndler EXCEPTION  "+e.toString());
        }
    }
    public void handleLogOutButt() {
        try {
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = String.format("YOU HAVE LOGGED OUT");
            secret=true;
            //SAVE DATA@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show("LOGGED OUT", endMessage);
            System.out.println("PASSWORDS1:"+(getPasswordFld().getText())+"PASSWORDS2:"+(controller.password));





        }catch(NullPointerException e){
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("ERROR unfilled areas","Please fill out all the boxes before pressing the Button");
            System.out.println("NewButt HAndler EXCEPTION  "+e.toString());
        }
    }


















    //   *******************************************************
    //************************SETTERS********************************

    public void saveBut() {
        try {
            controller.handleSaveRequest();
            System.out.println("save");
        }catch(IOException e){
            System.out.println("Error in saveBut");
        }

    }
    public void loadBut() {
        matrixGui.setStyle("-fx-background-color: gray;");
        System.out.println("load");
        initFigurePane(optionsGui, matrixGui);
    }




    public void modeBut() {

    }


    public void pauseBut(boolean val) {
        if (val) {
            matrixGui.setStyle("-fx-background-color: blue;");
            pauseRedoMatrixButton(lettersSetup(board));
            placeInTitle(textMaker("Game Paused", 17));
            initTitles(title, subtitle1, subtitle2);
            System.out.println("pause");
        } else {
            matrixGui.setStyle("-fx-background-color: red;");
            gameBoardGenerator(board);
            placeInTitle(textMaker("Game Resumed", 17));
            initTitles(title, subtitle1, subtitle2);
            System.out.println("resume");
        }
        initFigurePane(optionsGui, matrixGui);
        noStart();
    }


    public void start() {
        matrixGui.setStyle("-fx-background-color: pink;");
        Text currentMode = textMaker("mode",17);//(getModeBox().getValue().toString(),17);
        Text currentLevel = textMaker("level",17);//(getLevelBox().getValue().toString(),17);
        gettitles().getChildren().setAll(currentMode, currentLevel);
        keyWord=findWordFromList();
        gameBoardGenerator("123456789abcdefg");

        initFigurePane(optionsGui, matrixGui);
        initBodyPane(figurePane, gameTextsPane);
        System.out.println("start");
    }
















































    private void setupHandlers() {
        newButt.setOnAction                     (e -> handleNewButt());
        logInButt.setOnAction                   (e -> handleLogInButt());
        logOutButt.setOnAction                  (e -> handleLogOutButt());
        editUsernameButt.setOnAction            (e -> handleNameButt());
        editPasswordButt.setOnAction            (e -> handleKeyButt());








        startGame.setOnMouseClicked             (e -> controller.start());



        replayButt.setOnAction                  (e -> handleLogInButt());
    }
    public void homeBut() {
        Text currentMode = new Text("    ");
        Text currentLevel = new Text("      ");
        gettitles().getChildren().setAll(currentMode, currentLevel);
        gameBoardGenerator("BUZZDRODA EMAG W");
        //gameBoardGenerator("123456789abcdefg");
        System.out.println("home");
        initFigurePane(optionsGui, matrixGui);
        initBodyPane(figurePane, gameTextsPane);
        yesStart();
    }
    public void newBut() {

        Text currentMode  = textMaker("WELCOME TO BUZZWORD  ",30);
        Text currentLevel = textMaker("create a new account accoutn below and then Log--In",17);
        gettitles().getChildren().setAll(currentMode, currentLevel);
        //
        String[][] clear = new String[4][4];
        redoMatrixText(clear);
        matrixGui.setPadding(new Insets(5));
        matrixGui.setHgap(5);
        matrixGui.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(99);
        ColumnConstraints column2 = new ColumnConstraints(49, 149, 299);
        column2.setHgrow(Priority.ALWAYS);
        matrixGui.getColumnConstraints().addAll(column1, column2);
        Label userNameLbl = new Label("User name");
        Label passwordLbl = new Label("Password");
        Label passwordLbl2 = new Label("Re-Enter Password");
        // Username field
        GridPane.setHalignment(userNameLbl, HPos.RIGHT);
        matrixGui.add(userNameLbl, 0, 0);
        // Password field
        GridPane.setHalignment(passwordLbl, HPos.RIGHT);
        matrixGui.add(passwordLbl, 0, 1);
        GridPane.setHalignment(passwordLbl2, HPos.RIGHT);
        matrixGui.add(passwordLbl2, 0, 2);
        GridPane.setHalignment(userNameFld, HPos.LEFT);
        matrixGui.add(userNameFld, 1, 0);
        GridPane.setHalignment(passwordFld, HPos.LEFT);
        matrixGui.add(passwordFld, 1, 1);
        GridPane.setHalignment(passwordFld2, HPos.LEFT);
        matrixGui.add(passwordFld2, 1, 2);
        GridPane.setHalignment(newButt, HPos.RIGHT);
        matrixGui.add(newButt, 1, 3);
        initFigurePane(optionsGui, matrixGui);

        noStart();
    }
    public void handleNewButt() {
        try {
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            gui.getFileController().handleNewRequest();

            if (!((getPasswordFld().getText()).equals(getPasswordFld2().getText()))) {
                error.show("Password Error","Your passwords do not match please try again");
                System.out.println("PASSWORDS1:"+(getPasswordFld().getText())+"PASSWORDS2:"+(getPasswordFld2().getText()));
            }else {
                error.show("New User","Congratulation on making an account "+getUserNameFld().getText()+"\n PLEASE LOG IN NOW\n(by pressing the button with a log on it)");
                controller. handleNewUserSaveRequest();
            }
            matrixGui.setStyle("-fx-background-color: brown;");
        }catch(NullPointerException e){
            AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("ERROR unfilled areas","Please fill out all the boxes before pressing the Button");
            System.out.println("NewButt HAndler EXCEPTION  "+e.toString());
        }
    }
    public void handleNameButt() {
        controller.setName(getUserNameFld().getText());
        AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
            error.show("Name Updated","Your name is now: "+getUserNameFld().getText());
    }
    public void handleKeyButt() {
        controller.setkey(getPasswordFld().getText());
        AppMessageDialogSingleton error = AppMessageDialogSingleton.getSingleton();
        error.show("Name Updated","Your Password is now: "+getPasswordFld().getText());
    }
    public void logOutBut() {
        Text currentMode  = textMaker("Do you want to log out?  ",30);
        Text currentLevel = textMaker("",17);
        gettitles().getChildren().setAll(currentMode, currentLevel);
        String[][] clear = new String[ matrixSize1D][ matrixSize1D];
        redoMatrixText(clear);
        matrixGui.setPadding(new Insets(5));
        matrixGui.setHgap(5);
        matrixGui.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(99);
        ColumnConstraints column2 = new ColumnConstraints(49, 149, 299);
        column2.setHgrow(Priority.ALWAYS);
        matrixGui.getColumnConstraints().addAll(column1, column2);

        // Button
        GridPane.setHalignment(logOutButt, HPos.RIGHT);
        matrixGui.add(logOutButt, 0, 1);
        initFigurePane(optionsGui, matrixGui);
        noStart();
    }
    public void logInBut() {
        Text currentMode  = textMaker("WELCOME TO BUZZWORD  ",30);
        Text currentLevel = textMaker("If you have an account please Log--In",17);
        gettitles().getChildren().setAll(currentMode, currentLevel);
        String[][] clear = new String[ matrixSize1D][ matrixSize1D];
        redoMatrixText(clear);
        matrixGui.setPadding(new Insets(5));
        matrixGui.setHgap(5);
        matrixGui.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(99);
        ColumnConstraints column2 = new ColumnConstraints(49, 149, 299);
        column2.setHgrow(Priority.ALWAYS);
        matrixGui.getColumnConstraints().addAll(column1, column2);
        Label userNameLbl = new Label("User name");
        Label passwordLbl = new Label("Password");
        GridPane.setHalignment(userNameLbl, HPos.RIGHT);
        matrixGui.add(userNameLbl, 0, 0);
        // Password field
        GridPane.setHalignment(passwordLbl, HPos.RIGHT);
        matrixGui.add(passwordLbl, 0, 1);
        // Username field
        GridPane.setHalignment(userNameFld, HPos.LEFT);
        matrixGui.add(userNameFld, 1, 0);
        // Password field
        GridPane.setHalignment(passwordFld, HPos.LEFT);
        matrixGui.add(passwordFld, 1, 1);
        GridPane.setHalignment(logInButt, HPos.RIGHT);
        matrixGui.add(logInButt, 1, 3);
        initFigurePane(optionsGui, matrixGui);
        noStart();
    }
    public void helpBut() {
        TextArea taDescription = new TextArea();
        taDescription.setFont(new Font("Serif", 16));
        taDescription.setWrapText(true);
        taDescription.setEditable(false);

        help      = new ScrollPane(taDescription);

        String description = "1) Where is the log in?\n It is the button with a tree stump" +
                "\n\n2)How do I check my profile?\n You log in and then click the profile button which is the left most button"+
                "\n\n2)How do I log out?\n  Go to the button with a tree stump and click the log out button"+
                "\n\n2)How do I Play?\n You log in and then got to the home screen and press start or go to your library and pick a level"+
                "\n\n2)How do I win?\n You must get a certain amunt of point in under a minute"+
                "\n\n2)How do I play?\n You find as many words as you can in the grid provided"+
                "\n\n2)What is buzzword?\n a game similar to Boggle"+
                "\n\n2)Who is the creator?\n dinausor professor";
        taDescription.setText(description);



        help.     setFitToHeight(true);
        help.     setFitToWidth(true);
        help.     setStyle("-fx-background-color: green;");
        help.     setPadding(new Insets(20, 10, 20, 10));
        help.setVisible(true);
        if (bodyPane==null)bodyPane        = new HBox();
        bodyPane.       setStyle("-fx-background-color: green;");
        bodyPane.       setPadding(new Insets(20, 0, 20, 0));
        HBox.setHgrow(help, Priority.ALWAYS);
        bodyPane.getChildren().setAll(help, gameTextsPane);
    }
    public void profileBut(boolean secret) {
        if(secret){
            System.out.println("profile-----secret");
            Text currentMode  = textMaker("YOU NEED TO LOG IN TO SEE INFO  ",27);
            Text currentLevel = textMaker("click the button with a tree",15);
            gettitles().getChildren().setAll(currentMode, currentLevel);
            initFigurePane(optionsGui, matrixGui);
            noStart();
        }
        else{
            Text currentMode  = textMaker("Hello "+getUserNameFld().getText(),30);
            Text currentLevel = textMaker("you can edit your info below",17);
            gettitles().getChildren().setAll(currentMode, currentLevel);
            String[][] clear = new String[4][4];
            redoMatrixText(clear);
            matrixGui.setPadding(new Insets(5));
            matrixGui.setHgap(5);
            matrixGui.setVgap(5);
            ColumnConstraints column1 = new ColumnConstraints(99);
            ColumnConstraints column2 = new ColumnConstraints(49, 149, 299);
            column2.setHgrow(Priority.ALWAYS);
            matrixGui.getColumnConstraints().addAll(column1, column2);
            Label userNameLbl = new Label("User name: "+   controller.getName());
            Label passwordLbl = new Label("Password: "+controller.getkey());
            GridPane.setHalignment(userNameLbl, HPos.RIGHT);
            matrixGui.add(userNameLbl, 0, 0);
            GridPane.setHalignment(passwordLbl, HPos.RIGHT);
            matrixGui.add(passwordLbl, 0, 1);
            GridPane.setHalignment(editUsernameButt, HPos.RIGHT);
            matrixGui.add(editPasswordButt, 2, 1);
            GridPane.setHalignment(userNameFld, HPos.LEFT);
            matrixGui.add(userNameFld, 1, 0);
            GridPane.setHalignment(passwordFld, HPos.LEFT);
            matrixGui.add(passwordFld, 1, 1);
            GridPane.setHalignment(editPasswordButt, HPos.RIGHT);
            matrixGui.add(editUsernameButt,2, 0);
            GridPane.setHalignment(newButt, HPos.RIGHT);
            noStart();

        }
        //System.out.println("LevelBox HAndler EMPTY");
    }
    public void reinitialize(String screen) {
        matrixGui   = new GridPane();
        switch(screen) {
            case "home"://Done
                homeBut();
                break;
            case "logOut":
                                                    logInBut();
                                                    break;
            case "logIn":
                                                    logOutBut();
                                                    break;
            case "new":
                newBut();
                break;
            case "mode":
                                                    modeBut();
                                                    break;
            case "profile":
                                                    profileBut(secret);
                                                    break;
            case "start":
                                                    controller.start();
                                                    break;
            case "help":
                                                    helpBut();
                                                    break;
            case "pause":
                                                    pause=!pause;
            case "play":
                                                    pauseBut(pause);
                                                    break;
            case "load":
                                                    loadBut();
                                                    break;
            case "save":
                                                    saveBut();
                                                    break;
            default:
                System.out.println("default");
        }
        initFigurePane(optionsGui,matrixGui);
    }
    private void initToolColumn(){
        if (toolColumn==null)toolColumn=new VBox();
        toolColumn.setStyle("-fx-background-color: blue;");
        toolColumn.setPadding(new Insets(20, 20, 20, 20));
    }
    private void initSubTitles(){
        if (title==null)title           = new HBox();
        if (subtitle1==null)subtitle1   = new HBox();
        if (subtitle2==null)subtitle2   = new HBox();
        title.      setStyle("-fx-background-color: yellow;");
        subtitle1.  setStyle("-fx-background-color: green;");
        subtitle2.  setStyle("-fx-background-color: brown;");
        title.      setPadding(new Insets(10, 20, 10, 20));
        subtitle1.  setPadding(new Insets(10, 20, 10, 20));
        subtitle2.  setPadding(new Insets(10, 20, 10, 20));
    }
    private void initTitles(Node title,Node subtitle1,Node subtitle2){
        try {
            if (titles == null) titles = new VBox();
            titles.setStyle("-fx-background-color: gray;");
            titles.setPadding(new Insets(20, 10, 20, 10));
            titles.getChildren().setAll(title, subtitle1, subtitle2);
        }catch(IllegalArgumentException e){
            System.out.println("you tried to add the same children");
        }
    }
    private void initOptionsGui(Node toolColumn, Node titles){
        if (optionsGui==null)optionsGui  = new HBox();
        optionsGui.     setStyle("-fx-background-color: red;");
        optionsGui.     setPadding(new Insets(20, 10, 20, 10));
        optionsGui.getChildren().addAll(toolColumn,titles);
    }
    private void initMatrixGui(){
        if (matrixGui==null)matrixGui   = new GridPane();
        matrixGui.      setStyle("-fx-background-color: white;");
        matrixGui.      setPadding(new Insets(20, 20, 20, 20));

        redoMatrixText(letters);
    }
    private void initFigurePane(Node optionsGui, Node matrixGui){
        if (figurePane==null)figurePane  = new VBox();
        figurePane.     setStyle("-fx-background-color: orange;");
        figurePane.getChildren().setAll(optionsGui,matrixGui);
    }
    private void initGuessedLetters(){
        if (guessedLetters==null)guessedLetters      = new ScrollPane();
        //guesses      = new VBox();
        guessedLetters.     setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        guessedLetters.     setPrefSize(450, 350);
        guessedLetters.     setFitToHeight(true);
        guessedLetters.     setStyle("-fx-background-color: black;");
        guessedLetters.     setPadding(new Insets(20, 10, 20, 10));
    }
    private void updateGuesses(ArrayList<String> guesses){
        TextArea words = new TextArea();
        words.setEditable(false);
        guessedLetters      = new ScrollPane(words);
        guessedLetters.     setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        guessedLetters.     setPrefSize(450, 350);
        guessedLetters.     setFitToHeight(true);
        guessedLetters.     setStyle("-fx-background-color: black;");
        guessedLetters.     setPadding(new Insets(20, 10, 20, 10));

        String list="Begin: \n---------------\n";
            for(String elem : guesses){
                list+= elem+"\n";
            }

        words.setText(list);



        guessedLetters.     setFitToHeight(true);
        guessedLetters.     setFitToWidth(true);
        guessedLetters.     setStyle("-fx-background-color: green;");
        guessedLetters.     setPadding(new Insets(20, 10, 20, 10));
        guessedLetters.     setVisible(true);
        if (bodyPane==null)bodyPane        = new HBox();
        bodyPane.       setStyle("-fx-background-color: green;");
        bodyPane.       setPadding(new Insets(20, 0, 20, 0));
        HBox.setHgrow(guessedLetters, Priority.ALWAYS);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);


    }
    private void initRemainingGuessBox(){
        if (remainingGuessBox==null)remainingGuessBox   = new HBox();
        remainingGuessBox.  setStyle("-fx-background-color: brown;");
        remainingGuessBox.  setPadding(new Insets(20, 10, 20, 10));
    }
    private void initBlBox(){
        blBox               =new VBox();
        blBox.              setStyle("-fx-background-color: blue;");
        blBox.              setPadding(new Insets(20, 10, 20, 10));
    }
    private void initGameTextPane(Node remainingGuessBox, Node guessedLetters, Node blBox){
        if (gameTextsPane==null)gameTextsPane       = new VBox();
        gameTextsPane.      setStyle("-fx-background-color: pink;");
        gameTextsPane.      setPadding(new Insets(20, 10, 20, 10));
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,blBox);
        gameTextsPane.setVisible(false);
    }
    private void initHeadPane(PropertyManager propertyManager){
        guiHeadingLabel     = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        guiHeadingLabel.    setStyle("-fx-background-color: blue;");
        headPane            = new HBox();
        headPane.           setStyle("-fx-background-color: red;");
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);
    }
    private void initBodyPane(Node figurePane, Node gameTextsPane){
        if (bodyPane==null)bodyPane        = new HBox();
        bodyPane.       setStyle("-fx-background-color: green;");
        bodyPane.       setPadding(new Insets(20, 0, 20, 0));
        HBox.setHgrow(figurePane, Priority.ALWAYS);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }
    private void initFootToolBar(){
        startGame = new Button("Start Playing");
        HBox blankBoxLeft       = new HBox();
        HBox blankBoxRight      = new HBox();
        HBox.                   setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.                   setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar             = new ToolBar(blankBoxLeft,startGame,blankBoxRight);
        footToolbar.            setStyle("-fx-background-color: gray;");
    }
    private void initWorkspace(Node headPane, Node bodyPane, Node footToolbar){
        workspace = new VBox();
        workspace.getChildren().setAll(headPane, bodyPane, footToolbar);
    }
    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        initToolColumn();
        initSubTitles();
        initTitles(title,subtitle1,subtitle2);
        initOptionsGui(toolColumn,titles);
        initMatrixGui();
        initFigurePane(optionsGui,matrixGui);
        initGuessedLetters();
        initRemainingGuessBox();
        initBlBox();
        initGameTextPane(remainingGuessBox, guessedLetters,blBox);
        initHeadPane(propertyManager);
        initBodyPane(figurePane, gameTextsPane);
        initFootToolBar();
        initWorkspace(headPane, bodyPane, footToolbar);
    }
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();
        gui.getAppPane()                                                    .setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass()                                .setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane()                                                .setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));
        ObservableList<Node> toolbarChildren                                = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass()                              .add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass()     .add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));
        workspace.getStyleClass()                                           .add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass()                                     .setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }
    //***************************************************************
    //    ******************************************************
}

package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static controller.HangmanController.GameState.*;
import static java.lang.Character.toLowerCase;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author  VICTOR TELLEZ
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED,
    }

    private GameState   gamestate;   // the state of the game being shown in the workspace

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private boolean     success;     // whether or not player was successful
    private Button      gameButton;  // shared reference to the "start game" button
    private Path        workFile;

    TextArea        targetPoints;
    TextArea        totalScore;
    TextArea        currentMode;
    TextArea        currentLevel;
    String x;
    private Text        timer                          = textMaker("TIMER",17);//MAKE MODULER$$$$$$$$$$$$$$$$$$$$$$$$
    private int         targetPointsNumber             =1;//MAKE MODULER$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    private int         totalScoreNumber               =00;//MAKE MODULER$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    int cMode=1;
    int cLevel=1;
    ArrayList guesses;
    public String password;
    boolean outOfTime;
    public int levelInMode1;
    public int levelInMode2;
    public int levelInMode3;
    public int levelInMode4;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public GameState getGamestate() {
        return gamestate;
    }
    public void setGameState(GameState gamestate) {this.gamestate = gamestate;}


    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }
    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }
    public void basicSetupofTextDisplay(){
        targetPoints = new TextArea();
        targetPoints.setFont(new Font("Serif", 10));
        targetPoints.setWrapText(true);
        targetPoints.setEditable(false);
        x = "Target Points: "+targetPointsNumber;
        targetPoints.setText(x);
        totalScore  = new TextArea();
        totalScore .setFont(new Font("Serif", 10));
        totalScore .setWrapText(true);
        totalScore .setEditable(false);
             x = "Total Score: "+totalScoreNumber;
        totalScore .setText(x);
       currentMode = new TextArea();
       currentMode.setFont(new Font("Serif", 16));
       currentMode.setWrapText(true);
       currentMode.setEditable(false);
        x = "Mode:  "+cMode;
       currentMode.setText(x);
        currentLevel  = new TextArea();
        currentLevel .setFont(new Font("Serif", 16));
        currentLevel .setWrapText(true);
        currentLevel .setEditable(false);
         x = "Level:  "+cLevel;
        currentLevel .setText(x);

    }
    public void updateWorkspace(String screen){
        ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize(screen);
        appTemplate.getGUI().updateWorkspaceToolbar(screen);
    }
    public Text textMaker(String text, int size){
        Text x=new Text(text);
        x.setFont(Font.font("Verdana", FontWeight.BOLD, size));
        return x;
    }
    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();
        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }
    private boolean promptToQuit() throws IOException {
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        yesNoCancelDialog.show("Exit Request","Do you really wish to close the application?");
        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))return true;
        return false;
    }
    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;

    }
    public void checkToUpdateLevelandMode(Workspace gameWorkspace){

        gameWorkspace.placeInSubtitle1(currentLevel);
        gameWorkspace.placeInSubtitle2(currentMode);
    }

    public void start() {
        basicSetupofTextDisplay();
        success = false;
        outOfTime=false;
        guesses = new ArrayList();
        totalScoreNumber=00000;
        x="Total Score: "+totalScoreNumber;
        totalScore.setText(x);
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        checkToUpdateLevelandMode(gameWorkspace);
        gameWorkspace.getGameTextsPane().setVisible(true);
        gameWorkspace.getblBox().getChildren().setAll(totalScore,new Label("\t\t\t\t\t\t\t\t\t"),targetPoints);
        setGameState(INITIALIZED_UNMODIFIED);
        updateWorkspace("home");
        play();
    }
    //
    private void end() {
        enableGameButton();
        if(success){
            setGameState(INITIALIZED_MODIFIED);
        } else{
            setGameState(ENDED);
        }

        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        appTemplate.getGUI().updateWorkspaceToolbar("home");


        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success) {
                endMessage = String.format("YOU LOST SUCKER...TRY THIS LEVEL AGAIN");
            }if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }
    //
    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer(){
            @Override
            public void handle(long now) {
                String guess = "*";
                //final TextArea[] textArea = new TextArea[1];

                //appTemplate.getGUI().updateWorkspaceToolbar("start");
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {





                    guess[0] = toLowerCase(event.getCharacter().charAt(0));
                    System.out.println(guess[0]);
                    success=guess[0] =='x';
                    if(Character.isLetter(guess[0])){
                        word.add(guess[0]);



                        ((Workspace) appTemplate.getWorkspaceComponent()).getRemainingGuessBox().getChildren().setAll(textArea[0]);


                    }else{



                        if(((Workspace)appTemplate.getWorkspaceComponent()).getWord().toLowerCase().equals(word.toString())){
                            totalScoreNumber=1;
                            //System.out.println("1@@++++"+((Workspace)appTemplate.getWorkspaceComponent()).getWord().toLowerCase());
                            //System.out.println("2@@++++"+word.toString());
                            //System.out.println("winner");
                        }
                        //System.out.println("1@@++++"+((Workspace)appTemplate.getWorkspaceComponent()).getWord().toLowerCase());
                        //System.out.println("2@@++++"+word.toString());
                        //System.out.println("try again");
                        //textArea[0] = new TextArea();
                        //textArea[0].setFont(new Font("Serif", 14));
                        //textArea[0].setWrapText(true);
                        //textArea[0].setText(word.toString());

                        ((Workspace) appTemplate.getWorkspaceComponent()).getGuessedLetters().setContent(textArea[0]);

                    }









                });
                if (success||outOfTime)stop();
            }
            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }










    @Override
   public void handleModeRequest(){
        if (true) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("mode");
            appTemplate.getGUI().updateWorkspaceToolbar("mode");

            enableGameButton();
        }
    }

    @Override
   public void handlePlayRequest(){
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        basicSetupofTextDisplay();
        checkToUpdateLevelandMode(gameWorkspace);
        gameWorkspace.getGameTextsPane().setVisible(true);
        gameWorkspace.getblBox().getChildren().setAll(totalScore,new Label("\t\t\t\t\t\t\t\t\t"),targetPoints);
        String screen="pause";
        updateWorkspace(screen);
        appTemplate.getGUI().updateWorkspaceToolbar("play");
    }
    @Override
    public void handleHelpRequest(){
        if (true) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("help");
            appTemplate.getGUI().updateWorkspaceToolbar("help");
            enableGameButton();
        }
    }
    @Override
    public void handleProfileRequest(){
        if (true) {
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("profile");
            appTemplate.getGUI().updateWorkspaceToolbar("profile");
            enableGameButton();
        }
    }


















    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());

        }
    }
    private void load(Path source) throws IOException {
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);
        workFile = source;

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
       gamedata = (GameData) appTemplate.getDataComponent();


    }
    public void handleSaveRequest(int highscore1,int highscore2,int highscore3,int highscore4)  {
        if (gamedata!=null){
            if(highscore1!=0){
                gamedata.addMode1(highscore1);
            }
            if(highscore2!=0){
                gamedata.addMode2(highscore2);
            }
            if(highscore3!=0){
                gamedata.addMode3(highscore3);
            }
            if(highscore4!=0){
                gamedata.addMode4(highscore4);
            }
        }else{
            String key=((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText();
            String name=((Workspace) appTemplate.getWorkspaceComponent()).getUserNameFld().getText();
            gamedata=new GameData(name, key);
            if(highscore1!=0){
                gamedata.addMode1(highscore1);
            }
            if(highscore2!=0){
                gamedata.addMode2(highscore2);
            }
            if(highscore3!=0){
                gamedata.addMode3(highscore3);
            }
            if(highscore4!=0){
                gamedata.addMode4(highscore4);
            }
            System.out.println("GAMEDATA EQUALED NULL" +"\n");
        }
    }
    public String getName(){
        if(gamedata.getUserName()==null){
            setName(((Workspace) appTemplate.getWorkspaceComponent()).getUserNameFld().getText());
            return((Workspace) appTemplate.getWorkspaceComponent()).getUserNameFld().getText();
        }else{
            return gamedata.getUserName();
        }

    }
    public String getkey(){
        if(gamedata.getPassword()==null){
            setkey(((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText());
            return ((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText();
        }else{
            return gamedata.getPassword();
        }

    }
    public void setName(String x){
        gamedata.setUserName(x);
    }
    public void setkey(String x){
        gamedata.setPassword(x);
    }

    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }
    @Override
    public void handleNewRequest() {
        ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("new");
        appTemplate.getGUI().updateWorkspaceToolbar("new");
        enableGameButton();
        }




























    @Override
    public void handleHomeRequest(){
        ensureActivatedWorkspace();                            // ensure workspace is activated
        ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("home");
        appTemplate.getGUI().updateWorkspaceToolbar("home");
        enableGameButton();
    }
    @Override
    public void handleExitRequest() {
        try {
            boolean exit;
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("pause");
            if (gamestate.equals(INITIALIZED_MODIFIED)) {exit = promptToSave();}
            else {exit = promptToQuit();}


            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }
    public void handleNewUserSaveRequest(){
        String key=((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText();
        String name=((Workspace) appTemplate.getWorkspaceComponent()).getUserNameFld().getText();
        gamedata=new GameData(name, key);
    }
    @Override
    public void handleSaveRequest() throws IOException{
        System.out.println("NNNNNNNNNNNNNNNNNNNNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO HANDLESAVEREQUEST HANGMANCONTROLLER");

        handleSaveRequest(7,7,7,7);
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }
    @Override
    public void handleLogRequest(){
        if (((Workspace) appTemplate.getWorkspaceComponent()).isSecret()) {
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("logOut");
            appTemplate.getGUI().updateWorkspaceToolbar("log");
            enableGameButton();
        }else{
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize("logIn");
            appTemplate.getGUI().updateWorkspaceToolbar("log");
            enableGameButton();
        }
    }

}


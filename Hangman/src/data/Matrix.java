package data;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Created by Tellez Gutierrez on 12/10/2016.
 */

public class Matrix {
    Node pointer;
    Node node1;
    Node node2;
    Node node3;
    Node node4;
    Node node5;
    Node node6;
    Node node7;
    Node node8;
    Node node9;
    Node node10;
    Node node11;
    Node node12;
    Node node13;
    Node node14;
    Node node15;
    Node node16;
    private boolean validPath;
    private boolean success;

    //1     2       3       4
    //5     6       7       8
    //9     10      11      12
    //13    14      15      16

    public Matrix(){// create tree start with root

      node1  = new Node(1 );
      node2  = new Node(2 );
      node3  = new Node(3 );
      node4  = new Node(4 );
      node5  = new Node(5 );
      node6  = new Node(6 );
      node7  = new Node(7 );
      node8  = new Node(8 );
      node9  = new Node(9 );
      node10 = new Node(10);
      node11 = new Node(11);
      node12 = new Node(12);
      node13 = new Node(13);
      node14 = new Node(14);
      node15 = new Node(15);
      node16 = new Node(16);

        //ROW-1
        node1.setEast(node2);
        node2.setEast(node3);
        node1.setEast(node4);
        node2.setWest(node1);
        node3.setWest(node2);
        node4.setWest(node3);

        //ROW-2
        node5.setEast(node6);
        node6.setEast(node7);
        node7.setEast(node8);
        node8.setWest(node7);
        node7.setWest(node6);
        node6.setWest(node5);

        //ROW-3
        node9.setEast (node10);
        node10.setEast(node11);
        node11.setEast(node12);
        node12.setWest(node11);
        node11.setWest(node10);
        node10.setWest(node9);
        //ROW-4
        node13.setEast(node14);
        node14.setEast(node15);
        node15.setEast(node16);
        node16.setWest(node15);
        node15.setWest(node14);
        node14.setWest(node13);


        //COLLUMN-1
        node1 .setSouth(node5 );
        node5 .setSouth(node9 );
        node9 .setSouth(node13);
        node13.setNorth(node9 );
        node9 .setNorth(node5 );
        node5 .setNorth(node1 );
        //COLLUMN-2
        node2 .setSouth(node6 );
        node6 .setSouth(node10);
        node10.setSouth(node14);
        node14.setNorth(node10);
        node10.setNorth(node6 );
        node6 .setNorth(node2 );
        //COLLUMN-3
        node3 .setSouth(node7 );
        node7 .setSouth(node11);
        node11.setSouth(node15);
        node15.setNorth(node11);
        node11.setNorth(node7 );
        node7 .setNorth(node3 );
        //COLLUMN-4
        node4 .setSouth(node8 );
        node8 .setSouth(node12);
        node12.setSouth(node16);
        node16.setNorth(node12);
        node12.setNorth(node8 );
        node8 .setNorth(node4 );

        //RD-DIAGONAL-1
        node1.setSouthEast (node6);
        node5.setSouthEast (node10);
        node9.setSouthEast (node14);
        node2.setSouthEast (node7);
        node6.setSouthEast (node11);
        node10.setSouthEast(node15);
        node3.setSouthEast (node8);
        node7.setSouthEast (node12);
        node11.setSouthEast(node16);


        //RD-DIAGONAL-2
        node8 .setNorthWest (node3);
        node12.setNorthWest (node7);
        node16.setNorthWest (node11);
        node7 .setNorthWest (node2);
        node11.setNorthWest (node6);
        node15.setNorthWest (node10);
        node6 .setNorthWest (node1);
        node10.setNorthWest (node5);
        node14.setNorthWest (node9);

        //RD-DIAGONAL-3
        node5.setNorthEast  (node2);
        node9.setNorthEast  (node6);
        node13.setNorthEast (node10);
        node6.setNorthEast  (node3);
        node10.setNorthEast (node7);
        node14.setNorthEast (node11);
        node7.setNorthEast  (node4);
        node11.setNorthEast (node8);
        node15.setNorthEast (node12);

        //RD-DIAGONAL-5
        node4.setSouthWest  (node7);
        node8.setSouthWest  (node11);
        node12.setSouthWest (node15);
        node3.setSouthWest  (node6);
        node7.setSouthWest  (node10);
        node11.setSouthWest (node14);
        node2.setSouthWest  (node5);
        node6.setSouthWest  (node9);
        node10.setSouthWest (node13);
       //SNAKE
        node1.setpSnake(node2);
        node2.setpSnake(node3);
        node3.setpSnake(node4);
        node4.setpSnake(node8);
        node8.setpSnake(node7);
        node7.setpSnake(node6);
        node6.setpSnake(node10);
        node10.setpSnake(node11);
        node11.setpSnake(node12);
        node12.setpSnake(node16);
        node16.setpSnake(node15);
        node15.setpSnake(node14);
        node14.setpSnake(node13);
        node13.setpSnake(node9);
        node9.setpSnake(node5);
        node5.setpSnake(node1);
        validPath= true;
        success=false;
        System.out.println("Grid created successfully!");
    }

    public Node findNode(int value){
        switch(value){
            case 1:
                return node1;
            case 2:
                return node2;
            case 3:
                return node3;
            case 4:
                return node4;
            case 5:
                return node5;
            case 6:
                return node6;
            case 7:
                return node7;
            case 8:
                return node8;
            case 9:
                return node9;
            case 10:
                return node10;
            case 11:
                return node11;
            case 12:
                return node12;
            case 13:
                return node13;
            case 14:
                return node14;
            case 15:
                return node15;
            case 16:
                return node16;
            default:
                Node x = new Node(66);
                return x;
        }
    }

    public void placeWord(String word) {
        try {
            int startCoorcinate = 5;
            int lengthOfWord = word.length();
            StackPane stackPaneClone;
            Rectangle blue;
            Rectangle green;
            Text content;
            pointer = findNode(startCoorcinate);
            for (int a = 0; a < lengthOfWord; a++) {
                stackPaneClone = new StackPane();
                blue = new Rectangle(50, 50, Color.LIGHTBLUE);
                green = new Rectangle(50, 50, Color.LIGHTGREEN);
                content = new Text(word.substring(a, a + 1));
                System.out.println(word.substring(a, a + 1));
                content.setVisible(true);
                blue.setVisible(true);
                green.setVisible(!true);
                stackPaneClone.getChildren().addAll(blue, green, content);
                    pointer.getpSnake().setContent(stackPaneClone);
                    System.out.println("Node++#" + pointer.getpSnake().getLabel());
                    System.out.println("Node++content" + word.substring(a, a + 1));
                    pointer.clone(pointer.getpSnake());
                    System.out.println("NextNode" + pointer.getpSnake().getLabel() + "\n::::::\n");
            }
            validPath = true;
            success = false;
            System.out.println("Grid created successfully!asjdhkad");
        } catch (CloneNotSupportedException e) {
            System.out.println("PLACEWORD didnot work-----MATRIX");
        }
    }

























































    public Node getNode1() {
        return node1;
    }
    public void setNode1(Node node1) {
        this.node1 = node1;
    }
    public Node getNode2() {
        return node2;
    }
    public void setNode2(Node node2) {
        this.node2 = node2;
    }
    public Node getNode3() {
        return node3;
    }
    public void setNode3(Node node3) {
        this.node3 = node3;
    }
    public Node getNode4() {
        return node4;
    }
    public void setNode4(Node node4) {
        this.node4 = node4;
    }
    public Node getNode5() {
        return node5;
    }
    public void setNode5(Node node5) {
        this.node5 = node5;
    }
    public Node getNode6() {
        return node6;
    }
    public void setNode6(Node node6) {
        this.node6 = node6;
    }
    public Node getNode7() {
        return node7;
    }
    public void setNode7(Node node7) {
        this.node7 = node7;
    }
    public Node getNode8() {
        return node8;
    }
    public void setNode8(Node node8) {
        this.node8 = node8;
    }
    public Node getNode9() {
        return node9;
    }
    public void setNode9(Node node9) {
        this.node9 = node9;
    }
    public Node getNode10() {
        return node10;
    }
    public void setNode10(Node node10) {
        this.node10 = node10;
    }
    public Node getNode11() {
        return node11;
    }
    public void setNode11(Node node11) {
        this.node11 = node11;
    }
    public Node getNode12() {
        return node12;
    }
    public void setNode12(Node node12) {
        this.node12 = node12;
    }
    public Node getNode13() {
        return node13;
    }
    public void setNode13(Node node13) {
        this.node13 = node13;
    }
    public Node getNode14() {
        return node14;
    }
    public void setNode14(Node node14) {
        this.node14 = node14;
    }
    public Node getNode15() {
        return node15;
    }
    public void setNode15(Node node15) {
        this.node15 = node15;
    }
    public Node getNode16() {
        return node16;
    }
    public void setNode16(Node node16) {
        this.node16 = node16;
    }
    public boolean isValidPath() {
        return validPath;
    }
    public void setValidPath(boolean valid) {
        this.validPath = valid;
    }
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
}

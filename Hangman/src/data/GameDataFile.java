package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * @author Ritwik Banerjee
 * @author  VICTOR TELLEZ
 */
public class GameDataFile implements AppFileComponent {

    public static final String USERNAME         = "USERNAME";
    public static final String PASSWORD         = "PASSWORD ";
    public static final String MODE1            = "MODE1";
    public static final String MODE2            = "MODE2";
    public static final String MODE3            = "MODE3";
    public static final String MODE4            = "MODE4";

    @Override
    public void saveData(AppDataComponent data, Path to) {
        GameData         gamedata    = (GameData) data;
        String userName= gamedata.getUserName();
        String password= gamedata.getPassword();
        ArrayList<GameData.Level> mode1= gamedata.getMode1();
        ArrayList<GameData.Level> mode2= gamedata.getMode2();
        ArrayList<GameData.Level> mode3= gamedata.getMode3();
        ArrayList<GameData.Level> mode4= gamedata.getMode4();


        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(to)) {
            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
            generator.writeStartObject();
            generator.writeStringField(USERNAME, gamedata.getUserName());
            generator.writeStringField(PASSWORD, gamedata.getPassword());
            generator.writeFieldName(MODE1);
                generator.writeStartArray(mode1.size());
                for (GameData.Level c : mode1)
                   generator.writeString(c.toString());
                generator.writeEndArray();
            generator.writeFieldName(MODE2);
                generator.writeStartArray(mode1.size());
                for (GameData.Level c: mode2)
                    generator.writeString(c.toString());
                generator.writeEndArray();
            generator.writeFieldName(MODE3);
                generator.writeStartArray(mode1.size());
                for (GameData.Level c: mode3)
                    generator.writeString(c.toString());
                generator.writeEndArray();
            generator.writeFieldName(MODE4);
                generator.writeStartArray(mode1.size());
                for (GameData.Level c: mode4)
                    generator.writeString(c.toString());
                generator.writeEndArray();

            generator.writeEndObject();
            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData gamedata = (GameData) data;

        //gamedata.reset();
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(from));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case USERNAME:
                        jsonParser.nextToken();
                        gamedata.setUserName(jsonParser.getValueAsString());
                        break;
                    case PASSWORD:
                        jsonParser.nextToken();
                        gamedata.setPassword(jsonParser.getValueAsString());
                        break;
                    case MODE1:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gamedata.addMode1(Integer.parseInt(jsonParser.getValueAsString()));
                        break;
                    case MODE2:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gamedata.addMode2(Integer.parseInt(jsonParser.getValueAsString()));
                        break;
                    case MODE3:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gamedata.addMode3(Integer.parseInt(jsonParser.getValueAsString()));
                        break;
                    case MODE4:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gamedata.addMode4(Integer.parseInt(jsonParser.getValueAsString()));
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to LOAD JSON data");
                }
            }
        }
    }
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }

}
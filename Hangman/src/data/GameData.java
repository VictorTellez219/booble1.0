package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

import java.util.ArrayList;

/**
 *  @author  VICTOR TELLEZ
 * @author Ritwik Banerjee
 */
public class GameData implements AppDataComponent {

    @Override
    public void reset() {
        System.out.println("RESET");
    }

    class Level{
        int highScore;
        public Level(int highscore){
            setHighScore(highscore);
        }
        public void setHighScore(int score){
            highScore = score;
        }
        public int getHighScore(){
            return this.highScore;
        }
        public String toString(){
            return Integer.toString(this.getHighScore());
        }
    }


    private AppTemplate       appTemplate;
    private String            userName;
    private String            password;
    private ArrayList<Level>  mode1;
    private ArrayList<Level>  mode2;
    private ArrayList<Level>  mode3;
    private ArrayList<Level>  mode4;

    public GameData(String userName,String password){
        init( userName,password);
    }
    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }
    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            //init(((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText());
            init();
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void addMode1(int Highscore) {
        Level level = new Level(Highscore);
        this.mode1.add(level);}
    public void addMode2(int Highscore) {
        Level level = new Level(Highscore);
        this.mode2.add(level);}
    public void addMode3(int Highscore) {
        Level level = new Level(Highscore);
        this.mode3.add(level);}
    public void addMode4(int Highscore) {
        Level level = new Level(Highscore);
        this.mode4.add(level);}

    public void init(String userName,String password) {
        System.out.println("THE STRING GIVEN TO THE init FUNCTION WAS: "+ password);
        setUserName(userName);
        setPassword(password);
        this.mode1 = new ArrayList<>();
        this.mode2 = new ArrayList<>();
        this.mode3 = new ArrayList<>();
        this.mode4 = new ArrayList<>();
    }
    public void init() {
        System.out.println("THE STRING GIVEN TO THE init FUNCTION WAS: "+ password);
        setUserName("user-name");
        setPassword("password");
        this.mode1 = new ArrayList<>();
        this.mode2 = new ArrayList<>();
        this.mode3 = new ArrayList<>();
        this.mode4 = new ArrayList<>();
    }

    public void reset(int mode, int level) {
        switch (mode) {
            case 1:
                System.out.println("Mode: "+mode+"\tLevel "+level);
                break;
            case 2:
                System.out.println("Mode: "+mode+"\tLevel "+level);
                break;
            case 3:
                System.out.println("Mode: "+mode+"\tLevel "+level);
                break;
            case 4:
                System.out.println("Mode: "+mode+"\tLevel "+level);
                break;


       // init(((Workspace) appTemplate.getWorkspaceComponent()).getPasswordFld().getText());
        //appTemplate.getWorkspaceComponent().reloadWorkspace();
        }
    }

    public String               getUserName() {
        return userName;
    }
    public String               getPassword() {
        return password;
    }
    public ArrayList<Level>     getMode1() {
        return mode1;
    }
    public ArrayList<Level>     getMode2() {
        return mode2;
    }
    public ArrayList<Level>     getMode3() {
        return mode3;
    }
    public ArrayList<Level>     getMode4() {
        return mode4;
    }

    public void     setUserName(String Name) {
        this.userName = Name;
    }
    public void     setPassword(String password) {
        this.password = password;
    }
    public GameData setPasswordRGameData(String targetWord) {
        this.password = targetWord;
        return this;
    }
    public GameData setMode1(ArrayList<Level> mode1) {
        this.mode1=mode1;
        return this;
    }
    public GameData setMode2(ArrayList<Level> mode2) {
        this.mode2=mode2;
        return this;
    }
    public GameData setMode3(ArrayList<Level> mode3) {
        this.mode3=mode3;
        return this;
    }
    public GameData setMode4(ArrayList<Level> mode4) {
        this.mode4=mode4;
        return this;
    }



}
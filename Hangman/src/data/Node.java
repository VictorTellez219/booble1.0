package data;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

/**
 * Created by Tellez Gutierrez on 12/10/2016.
 */

public class Node  {

    private int label;//The "name" of this TreeNode.
    private StackPane content;
    private Node west;
    private Node east;
    private Node north;
    private Node south;
    private Node northWest;
    private Node northEast;
    private Node southWest;
    private Node southEast;
    private Node pSnake;
    private boolean isThereAwest;
    private boolean isThereAeast;
    private boolean isThereAnorth;
    private boolean isThereAsouth;
    private boolean isThereAnorthWest;
    private boolean isThereAnorthEast;
    private boolean isThereAsouthWest;
    private boolean isThereAsouthEast;
    private boolean visited;


    public Node(int name) {
        StackPane stackPaneGhost= new StackPane();
        Rectangle blue = new Rectangle(50, 50, Color.RED);
        Rectangle green = new Rectangle(50, 50, Color.WHITE);
        Text letter = new Text("*");
        letter.setVisible(true);
        blue.setVisible(true);
        green.setVisible(!true);
        stackPaneGhost.getChildren().addAll(blue, green, letter);
        label               =name;
        content             =stackPaneGhost;
        west                =null;
        east                =null;
        north               =null;
        south               =null;
        northWest           =null;
        northEast           =null;
        southWest           =null;
        southEast           =null;
        pSnake              =null;
        isThereAwest        =false;
        isThereAeast        =false;
        isThereAnorth       =false;
        isThereAsouth       =false;
        isThereAnorthWest   =false;
        isThereAnorthEast   =false;
        isThereAsouthWest   =false;
        isThereAsouthEast   =false;
        visited             =false;


    }



    public Node clone(Node node) throws CloneNotSupportedException {
        this.label               = node.getLabel();
        this.content             =  node.getContent();
        this.west                = node.getWest();
        this.east                = node.getEast();
        this.north               = node.getNorth();
        this.south               = node.getSouth();
        this.northWest           = node.getNorthWest();
        this.northEast           = node.getNorthEast();
        this.southWest           = node.getSouthWest();
        this.southEast           = node.getSouthEast();
        this.pSnake              = node.getpSnake();
        this.isThereAwest        = node.isThereAwest();
        this.isThereAeast        = node.isThereAeast();
        this.isThereAnorth       = node.isThereAnorth();
        this.isThereAsouth       = node.isThereAsouth();
        this.isThereAnorthWest   = node.isThereAnorthWest();
        this.isThereAnorthEast   = node.isThereAnorthEast();
        this.isThereAsouthWest   = node.isThereAsouthWest();
        this.isThereAsouthEast   = node.isThereAsouthEast();
        visited             =false;

        return this;
    }

    public Node getpSnake() {
        return pSnake;
    }
    public void setpSnake(Node pSnake) {
        this.pSnake = pSnake;
    }
    public StackPane getContent() {
        return content;
    }
    public void setContent(StackPane content) {
        this.content = content;
    }
    public int getLabel() {
        return label;
    }
    public void setLabel(int label) {
        this.label = label;
    }
    public Node getWest() {
        return west;
    }
    public void setWest(Node west) {
        this.west = west;
        setThereAwest(true);
    }
    public Node getEast() {
        return east;
    }
    public void setEast(Node east) {
        this.east = east;
        setThereAeast(true);
    }
    public Node getNorth() {
        return north;
    }
    public void setNorth(Node north) {
        this.north = north;
        setThereAnorth(true);
    }
    public Node getSouth() {
        return south;
    }
    public void setSouth(Node south) {
        this.south = south;
        setThereAsouth(true);
    }
    public Node getNorthWest() {
        return northWest;
    }
    public void setNorthWest(Node northWest) {
        this.northWest = northWest;
        setThereAnorthWest(true);
    }
    public Node getNorthEast() {
        return northEast;
    }
    public void setNorthEast(Node northEast) {
        this.northEast = northEast;
        setThereAnorthEast(true);
    }
    public Node getSouthWest() {
        return southWest;
    }
    public void setSouthWest(Node southWest) {
        this.southWest = southWest;
        setThereAsouthWest(true);
    }
    public Node getSouthEast() {
        return southEast;
    }
    public void setSouthEast(Node southEast) {
        this.southEast = southEast;
        setThereAsouthEast(true);
    }
    public boolean isThereAwest() {
        return isThereAwest;
    }
    public void setThereAwest(boolean thereAwest) {
        isThereAwest = thereAwest;
    }
    public boolean isThereAeast() {
        return isThereAeast;
    }
    public void setThereAeast(boolean thereAeast) {
        isThereAeast = thereAeast;
    }
    public boolean isThereAnorth() {
        return isThereAnorth;
    }
    public void setThereAnorth(boolean thereAnorth) {
        isThereAnorth = thereAnorth;
    }
    public boolean isThereAsouth() {
        return isThereAsouth;
    }
    public void setThereAsouth(boolean thereAsouth) {
        isThereAsouth = thereAsouth;
    }
    public boolean isThereAnorthWest() {
        return isThereAnorthWest;
    }
    public void setThereAnorthWest(boolean thereAnorthWest) {
        isThereAnorthWest = thereAnorthWest;
    }
    public boolean isThereAnorthEast() {
        return isThereAnorthEast;
    }
    public void setThereAnorthEast(boolean thereAnorthEast) {
        isThereAnorthEast = thereAnorthEast;
    }
    public boolean isThereAsouthWest() {
        return isThereAsouthWest;
    }
    public void setThereAsouthWest(boolean thereAsouthWest) {
        isThereAsouthWest = thereAsouthWest;
    }
    public boolean isThereAsouthEast() {
        return isThereAsouthEast;
    }
    public void setThereAsouthEast(boolean thereAsouthEast) {
        isThereAsouthEast = thereAsouthEast;
    }
    public boolean isVisited() {
        return visited;
    }
    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public String toString(){
        return "Label: "+this.getLabel();
    }
}

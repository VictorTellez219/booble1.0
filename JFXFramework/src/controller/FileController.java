package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {

    void handleNewRequest();

    void handleSaveRequest() throws IOException;

    void handleLoadRequest() throws IOException;

    void handleExitRequest();

    void handleHomeRequest();

    void handleModeRequest();

    void handleLogRequest();

    void handlePlayRequest();

    void handleHelpRequest();

    void handleProfileRequest();
}

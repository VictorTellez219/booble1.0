package ui;

import apptemplate.AppTemplate;
import components.AppStyleArbiter;
import controller.FileController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class provides the basic user interface for this application, including all the file controls, but it does not
 * include the workspace, which should be customizable and application dependent.
 * * @author  VICTOR TELLEZ
 * @author Richard McKenna, Ritwik Banerjee
 */
public class AppGUI implements AppStyleArbiter {

    protected FileController fileController;   // to react to file-related controls
    protected Stage          primaryStage;     // the application window
    protected Scene          primaryScene;     // the scene graph
    protected BorderPane     appPane;          // the root node in the scene graph, to organize the containers
    protected FlowPane       toolbarPane;      // the top toolbar
    protected Button         newButton;        // button to create a new instance of the application
    protected Button         saveButton;       // button to save progress on application
    protected Button         loadButton;       // button to load a saved game from (json) file
    protected Button         exitButton;       // button to exit application
    protected Button         homeButton;
    protected Button         logButton;
    protected Button         modeButton;
    protected Button         playButton;
    protected Button         profileButton;
    protected Button         helpButton;
    protected String         applicationTitle; // the application title

    private int appWindowWidth;  // optional parameter for window width that can be set by the application
    private int appWindowHeight; // optional parameter for window height that can be set by the application
    
    /**
     * This constructor initializes the file toolbar for use.
     *
     * @param initPrimaryStage The window for this application.
     * @param initAppTitle     The title of this application, which
     *                         will appear in the window bar.
     * @param app              The app within this gui is used.
     */
    public AppGUI(Stage initPrimaryStage, String initAppTitle, AppTemplate app) throws IOException, InstantiationException {
        this(initPrimaryStage, initAppTitle, app, -1, -1);
    }

    public AppGUI(Stage primaryStage, String applicationTitle, AppTemplate appTemplate, int appWindowWidth, int appWindowHeight) throws IOException, InstantiationException {
        this.appWindowWidth = appWindowWidth;
        this.appWindowHeight = appWindowHeight;
        this.primaryStage = primaryStage;
        this.applicationTitle = applicationTitle;
        initializeToolbar();                    // initialize the top toolbar
        initializeToolbarHandlers(appTemplate); // set the toolbar button handlers
        initializeWindow();                     // start the app window (without the application-specific workspace)

    }

    public FileController getFileController() {
        return this.fileController;
    }

    public FlowPane getToolbarPane() { return toolbarPane; }

    public BorderPane getAppPane() { return appPane; }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     *
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    public void setPrimaryScene(Scene scene) {
        primaryScene =scene;
    }
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     *
     * @return This application's primary stage (i.e. window).
     */
    public Stage getWindow() { return primaryStage; }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initializeToolbar() throws IOException {
        toolbarPane = new FlowPane();
        profileButton = initializeChildButton(toolbarPane, PROFILE_ICON.toString(), PROFILE_TOOLTIP.toString(), false);
        homeButton    = initializeChildButton(toolbarPane, HOME_ICON.toString(),    HOME_TOOLTIP.   toString(), false);
        exitButton    = initializeChildButton(toolbarPane, EXIT_ICON.toString(),    EXIT_TOOLTIP.   toString(), false);
        helpButton    = initializeChildButton(toolbarPane, HELP_ICON.toString(),    HELP_TOOLTIP.   toString(), false);
        logButton     = initializeChildButton(toolbarPane, LOG_ICON.toString(),     LOG_TOOLTIP.    toString(), false);
        modeButton    = initializeChildButton(toolbarPane, MODE_ICON.toString(),    MODE_TOOLTIP.   toString(), false);
        newButton     = initializeChildButton(toolbarPane, NEW_ICON.toString(),     NEW_TOOLTIP.    toString(), false);
        playButton    = initializeChildButton(toolbarPane, PLAY_ICON.toString(),    PLAY_TOOLTIP.   toString(), false);
        saveButton    = initializeChildButton(toolbarPane, SAVE_ICON.toString(),    SAVE_TOOLTIP.   toString(), true );
        loadButton    = initializeChildButton(toolbarPane, LOAD_ICON.toString(),    LOAD_TOOLTIP.   toString(), false);

        newButton.      setStyle("-fx-background-color: snow;");
        loadButton.     setStyle("-fx-background-color: snow;");
        saveButton.     setStyle("-fx-background-color: snow;");
        exitButton.     setStyle("-fx-background-color: snow;");
        homeButton.     setStyle("-fx-background-color: snow;");
        logButton.      setStyle("-fx-background-color: snow;");
        modeButton.     setStyle("-fx-background-color: snow;");
        playButton.     setStyle("-fx-background-color: snow;");
        profileButton.  setStyle("-fx-background-color: snow;");
        helpButton.     setStyle("-fx-background-color: snow;");

        logButton.      setVisible(!true);
        helpButton.     setVisible(!true);
        exitButton.     setVisible(true);
        newButton.      setVisible(!true);
        modeButton.     setVisible(!true);
        loadButton.     setVisible(!true);
        homeButton.     setVisible(true);
        profileButton.  setVisible(!true);
        playButton.     setVisible(!true);
        saveButton.     setVisible(!true);
    }


    //BUTTON HANDLERS@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    private void initializeToolbarHandlers(AppTemplate app) throws InstantiationException {
        try {
            Method         getFileControllerClassMethod = app.getClass().getMethod("getFileControllerClass");
            String         fileControllerClassName      = (String) getFileControllerClassMethod.invoke(app);
            Class<?>       klass                        = Class.forName("controller." + fileControllerClassName);
            Constructor<?> constructor                  = klass.getConstructor(AppTemplate.class);
            fileController = (FileController) constructor.newInstance(app);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        newButton.setOnAction(e -> fileController.handleNewRequest());
        homeButton.setOnAction(e -> fileController.handleHomeRequest());
        modeButton.setOnAction(e -> fileController.handleModeRequest());
        logButton.setOnAction(e -> fileController.handleLogRequest());
        profileButton.setOnAction(e -> fileController.handleProfileRequest());
        playButton.setOnAction(e -> fileController.handlePlayRequest());
        helpButton.setOnAction(e -> fileController.handleHelpRequest());
        saveButton.setOnAction(e -> {
            try {fileController.handleSaveRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
                System.exit(1);
            }
        });
        loadButton.setOnAction(e -> {
            try {
                fileController.handleLoadRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        exitButton.setOnAction(e -> fileController.handleExitRequest());
    }

    public void updateWorkspaceToolbar(boolean savable) {
        saveButton.setDisable(!savable);
        newButton.setDisable(false);
        exitButton.setVisible(false);
        homeButton.setDisable(false);
        logButton.setDisable(false);
        modeButton.setDisable(false);
        playButton.setDisable(false);
        profileButton.setDisable(false);
        helpButton.setDisable(false);
    }
    public void updateWorkspaceToolbar(String screen) {
        homeButton.     setVisible(true);
        switch(screen) {
            case "home":

                            logButton.      setDisable(false);
                            modeButton.     setDisable(false);
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            newButton.      setDisable(false);
                            homeButton.     setDisable(!false);
                            saveButton.     setDisable(!false);
                            playButton.     setDisable(!false);
                            profileButton.  setDisable(!false);
                            logButton.      setVisible(true);
                            modeButton.     setVisible(true);
                            helpButton.     setVisible(true);
                            exitButton.     setVisible(true);
                            newButton.      setVisible(true);
                            playButton.     setVisible(!true);
                            profileButton.  setVisible(!true);
                            homeButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            break;

            case "log":
                            homeButton.     setDisable(false);
                            newButton.      setDisable(false);
                            profileButton.  setDisable(false);
                            logButton.      setDisable(false);
                            modeButton.     setDisable(false);
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            playButton.     setDisable(!false);
                            saveButton.     setDisable(!false);
                            newButton.      setVisible(true);
                            homeButton.     setVisible(true);
                            profileButton.  setVisible(true);
                            logButton.      setVisible(true);
                            modeButton.     setVisible(true);
                            helpButton.     setVisible(true);
                            exitButton.     setVisible(true);
                            playButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            break;

            case "mode":
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            logButton.      setDisable(false);
                            homeButton.     setDisable(false);
                            profileButton.  setDisable(false);
                            newButton.      setDisable(!false);
                            modeButton.     setDisable(!false);
                            saveButton.     setDisable(!false);
                            playButton.     setDisable(!false);
                            profileButton.  setVisible(true);
                            logButton.      setVisible(true);
                            helpButton.     setVisible(true);
                            exitButton.     setVisible(true);
                            homeButton.     setVisible(true);
                            homeButton.     setVisible(true);
                            newButton.      setVisible(!true);
                            modeButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            playButton.     setVisible(!true);
                            break;

            case "play":
                            profileButton.  setDisable(false);
                            logButton.      setDisable(false);
                            modeButton.     setDisable(false);
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            saveButton.     setDisable(false);
                            homeButton.     setDisable(false);
                            playButton.     setDisable(false);
                            newButton.      setDisable(!false);
                            homeButton.     setVisible(true);
                            profileButton.  setVisible(true);
                            logButton.      setVisible(true);
                            modeButton.     setVisible(true);
                            helpButton.     setVisible(true);
                            exitButton.     setVisible(true);
                            saveButton.     setVisible(true);
                            playButton.     setVisible(true);
                            newButton.      setVisible(!true);
                            break;

            case "profile":
                            saveButton.     setDisable(!false);
                            playButton.     setDisable(!false);
                            modeButton.     setDisable(!false);
                            profileButton.  setDisable(!false);
                            newButton.      setDisable(!false);
                            logButton.      setDisable(false);
                            homeButton.     setDisable(false);
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            logButton.      setVisible(true);
                            helpButton.     setVisible(true);
                            exitButton.     setVisible(true);
                            homeButton.     setVisible(true);
                            profileButton.  setVisible(!true);
                            newButton.      setVisible(!true);
                            modeButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            playButton.     setVisible(!true);
                            break;

            case "help":
                            profileButton.  setDisable(!false);
                            newButton.      setDisable(!false);
                            logButton.      setDisable(!false);
                            modeButton.     setDisable(!false);
                            helpButton.     setDisable(!false);
                            saveButton.     setDisable(!false);
                            playButton.     setDisable(!false);
                            exitButton.     setDisable(false);
                            homeButton.     setDisable(false);
                            exitButton.     setVisible(true);
                            homeButton.     setVisible(true);
                            profileButton.  setVisible(!true);
                            newButton.      setVisible(!true);
                            logButton.      setVisible(!true);
                            modeButton.     setVisible(!true);
                            helpButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            playButton.     setVisible(!true);
                            break;


            case "new":
                            saveButton.     setDisable(!false);
                            playButton.     setDisable(!false);
                            profileButton.  setDisable(!false);
                            newButton.      setDisable(!false);
                            logButton.      setDisable(false);
                            modeButton.     setDisable(!false);
                            helpButton.     setDisable(false);
                            exitButton.     setDisable(false);
                            homeButton.     setDisable(false);
                            exitButton.     setVisible(true);
                            homeButton.     setVisible(true);
                            helpButton.     setVisible(true);
                            profileButton.  setVisible(!true);
                            newButton.      setVisible(!true);
                            logButton.      setVisible(true);
                            modeButton.     setVisible(!true);
                            saveButton.     setVisible(!true);
                            playButton.     setVisible(!true);
                            break;
            default:
                saveButton.     setDisable(false);
                playButton.     setDisable(false);
                profileButton.  setDisable(false);
                newButton.      setDisable(false);
                logButton.      setDisable(false);
                modeButton.     setDisable(false);
                helpButton.     setDisable(false);
                exitButton.     setDisable(false);
                homeButton.     setDisable(false);
                exitButton.     setVisible(true);
                homeButton.     setVisible(true);
                helpButton.     setVisible(true);
                profileButton.  setVisible(true);
                newButton.      setVisible(true);
                logButton.      setVisible(true);
                modeButton.     setVisible(true);
                saveButton.     setVisible(true);
                playButton.     setVisible(true);
                break;
        }
    }


    private void initializeWindow() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        // SET THE WINDOW TITLE
        primaryStage.setTitle(applicationTitle);

        // add the toolbar to the constructed workspace
        appPane = new BorderPane();
        appPane.setTop(toolbarPane);
        primaryScene = appWindowWidth < 1 || appWindowHeight < 1 ? new Scene(appPane)
                                                                 : new Scene(appPane,
                                                                             appWindowWidth,
                                                                             appWindowHeight);
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resrouces folder does not exist.");
        try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
            primaryStage.getIcons().add(new Image(appLogoStream));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     *
     * @param toolbarPane Toolbar pane into which to place this button.
     * @param icon        Icon image file name for the button.
     * @param tooltip     Tooltip to appear when the user mouses over the button.
     * @param disabled    true if the button is to start off disabled, false otherwise.
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initializeChildButton(Pane toolbarPane, String icon, String tooltip, boolean disabled) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");
        Button button = new Button();
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon)))) {
            Image buttonImage = new Image(imgInputStream);
            button.setDisable(disabled);
            button.setGraphic(new ImageView(buttonImage));
            Tooltip buttonTooltip = new Tooltip(propertyManager.getPropertyValue(tooltip));
            button.setTooltip(buttonTooltip);
            toolbarPane.getChildren().add(button);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return button;
    }
    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    @Override
    public void initStyle() {
        // currently, we do not provide any stylization at the framework-level
    }
    public Stage getPrimaryStage() {
        return primaryStage;
    }
    public Button getHomeButton() {
        return homeButton;
    }
}
